import * as winston from "winston";

import { IWorker } from "./Worker";
import { IExaminationsClient } from "../Db/ExaminationsClient";
import { AssignJobCommand, CancelJobCommand, JobFinished } from "./Commands";

export interface IJobControl {
    jobFinished?: (event: JobFinished) => void;
    assignJob(cmd: AssignJobCommand): void;
    cancelJob(cmd: CancelJobCommand): void;
}

export class JobControl implements IJobControl {
    private runningJobs: { [refNumber: string]: boolean | undefined; } = {};

    jobFinished?: (event: JobFinished) => void;

    constructor(
        private readonly worker: IWorker,
        private readonly client: IExaminationsClient) {
    }

    assignJob(cmd: AssignJobCommand): void {
        winston.info("Assigning new job %s - calculating %s for exam %s, protocol %s", cmd.refNumber, cmd.paramName, cmd.examId, cmd.protoId);
        this.runningJobs[cmd.refNumber] = false;
        setImmediate(this.runCalculation.bind(this), cmd);
    }

    cancelJob(cmd: CancelJobCommand): void {
        if (this.runningJobs[cmd.refNumber] !== undefined) {
            winston.info("Canceling job %s", cmd.refNumber);
            this.runningJobs[cmd.refNumber] = true;
        }
    }

    private async runCalculation(cmd: AssignJobCommand) {
        winston.verbose("Running calculation of job %s", cmd.refNumber);
        const result = await this.worker.compute(cmd.protoId, cmd.examId, cmd.paramName);
        if (result.type === "Computed") {
            winston.verbose("Calculation of job %s finished, saving the result", cmd.refNumber);
            setImmediate(this.saveResult.bind(this), cmd, result.data);
        } else {
            winston.warn("Cannot calculate job %s, calculation error: '%s', exception: %s",
                cmd.refNumber, result.message, result.innerError !== undefined ? result.innerError.message : "");
            setImmediate(this.saveResult.bind(this), cmd, { ...result, type: "Error" });
        }
    }

    private async saveResult(cmd: AssignJobCommand, result: Object) {
        let canceled = this.runningJobs[cmd.refNumber] || false;
        if (!canceled) {
            let dbResult = await this.client.setParameter(cmd.protoId, cmd.examId, cmd.paramName, result);
            if (dbResult.type === "DbActionError") {
                winston.warn("Cannot update the parameter %s of examination %s, protocol %s, (result of job %s), database returned an error: %s",
                    cmd.paramName, cmd.examId, cmd.protoId, cmd.refNumber, dbResult.error);
            } else {
                winston.info("Parameter %s of examination %s, protocol %s updated (result of job %s)",
                    cmd.paramName, cmd.examId, cmd.protoId, cmd.refNumber);
            }
        }

        if (this.jobFinished) {
            this.jobFinished({ type: "JobFinished", canceled, refNumber: cmd.refNumber });
        }
        delete this.runningJobs[cmd.refNumber];
    }

}
