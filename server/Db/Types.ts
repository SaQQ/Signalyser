import { ObjectID } from "mongodb";

import {
    SignalDescriptor,
    StageDescriptor, TagDescriptor,
    ParameterDescriptor, ProtocolDescription,
    SignalData, TagValue,
    AttributesCollection
} from "../Contract";

export interface DbProtocol {
    _id: ObjectID;
    name: string;
    signals: SignalDescriptor[];
    stages: StageDescriptor[];
    tags: TagDescriptor[];
    params: ParameterDescriptor[];
}

export interface DbExamination {
    _id: ObjectID;
    tags: AttributesCollection<TagValue>;
    params: AttributesCollection<Object>;
}
