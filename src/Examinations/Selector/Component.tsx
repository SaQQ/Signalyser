import * as React from "react";
import * as immutable from "immutable";

import IconButton from "material-ui/IconButton";
import FlatButton from "material-ui/FlatButton";
import Subheader from "material-ui/Subheader";
import { List, ListItem } from "material-ui/List";
import { Toolbar, ToolbarGroup } from "material-ui/Toolbar";
import Chip from "material-ui/Chip";
import Avatar from "material-ui/Avatar";
import { indigo900, blue300 } from "material-ui/styles/colors";

import NavigationRefresh from "material-ui/svg-icons/navigation/refresh";
import Redo from "material-ui/svg-icons/content/redo";
import ModeEdit from "material-ui/svg-icons/editor/mode-edit";

import * as consts from "../../Consts";

import { Header, LoadingScreen, StatefulTextField, NotFound } from "../../Controls";
import { AppState } from "../../State";
import { Dispatch, connect } from "../../Utils";

import { loadExaminations, changeFilter, recomputeProtocol } from "./Actions";
import { editExamination, openNewExamination } from "../Navigation";

import { ExaminationDescription } from "../../Contract";

import { parse, SyntaxError } from "../../Client/grammar";

function shouldDisplay(param: any) {
    return param !== undefined && param.type !== "Error";
}

interface ExaminationEntryProps {
    protocolId: string;
    examination: ExaminationDescription;

    openExamination: (protocolId: string, examId: string) => void;
}

const ExaminationEntry = (props: ExaminationEntryProps) => {
    // TODO: this should be done in reducer, not here
    let hasTags = Object.keys(props.examination).indexOf("tags") !== -1;
    let hasParams = Object.keys(props.examination).indexOf("params") !== -1;
    let tags = hasTags ? Object.keys(props.examination.tags).map(t => ({
        name: t,
        label: t.substring(0, 2),
        value: props.examination.tags[t]
    })) : [];
    let params = hasParams ? Object.keys(props.examination.params)
        .filter(p => shouldDisplay(props.examination.params[p]))
        .map(p => ({
            name: p,
            label: p.substring(0, 2),
            value: JSON.stringify(props.examination.params[p])
        })) : [];
    return <ListItem
        onTouchTap={() => props.openExamination(props.protocolId, props.examination.id)}>
        <div style={{ display: "flex", flexWrap: "wrap" }}>
            { tags.length === 0 ? <Chip style={{ margin: 2 }} key="no-tags-chip">No Tags</Chip> :
            tags.map(t =>
                <Chip style={{ margin: 2 }} key={t.name} >
                    <Avatar size={32}>{t.label}</Avatar>
                    {t.value}
                </Chip>
            )}
            {params.length === 0 ? <Chip style={{ margins: 2 }} key="no-params-chip">No Parameters</Chip> :
            params.map(p =>
                <Chip backgroundColor={blue300} style={{ margin: 2 }} key={p.name} >
                    <Avatar color={blue300} backgroundColor={indigo900} size={32}>{p.label}</Avatar>
                    {p.value}
                </Chip>
            )}
        </div>
    </ListItem>;
};

export interface ExaminationSelectorProps {
    protocolId: string;
    isLoading: boolean;
    examinations: immutable.List<ExaminationDescription>;
    filterText: string;
    filter: any;
    filterErrorText: string;
}

export interface ExaminationSelectorActions {
    refreshData: (protocolId: string, filter: string) => void;
    openExamination: (protocolId: string, examId: string) => void;
    newExamination: (protocolId: string) => void;
    changeFilter: (filterText: string, filter: any, errorText: string) => void;
    recomputeProtocol: (protocolId: string) => void;
}

class ExaminationSelectorControl extends React.Component<ExaminationSelectorProps & ExaminationSelectorActions, {}> {

    componentWillMount() {
        this.props.refreshData(this.props.protocolId, this.props.filter);
    }

    render() {
        let refresh = () => this.props.refreshData(this.props.protocolId, this.props.filter);
        let addExamination = () => this.props.newExamination(this.props.protocolId);
        let recomputeProtocol = () => this.props.recomputeProtocol(this.props.protocolId);

        return <div>
            <Header title="Select examination">
                <Toolbar>
                    <ToolbarGroup firstChild={true}>
                        <StatefulTextField
                            value={this.props.filterText}
                            errorText={this.props.filterErrorText}
                            style={consts.TagBarTextFieldStyle}
                            inputStyle={consts.TagBarControlStyle}
                            hintText="Filter examinations"
                            onValueChange={v => {
                                try {
                                    let filter = parse(v.toString()) as Object;
                                    this.props.changeFilter(v.toString(), filter, "");
                                } catch (e) {
                                    this.props.changeFilter(v.toString(), {}, e.toString());
                                }
                            } } />
                        <IconButton disabled={this.props.filterErrorText !== ""} onTouchTap={refresh} tooltip="Refresh"><NavigationRefresh {...consts.TagBarControlStyle} /></IconButton>
                        <IconButton onTouchTap={recomputeProtocol} tooltip="Recompute protocol"><Redo {...consts.TagBarControlStyle} /></IconButton>
                    </ToolbarGroup>
                    <ToolbarGroup lastChild={true}>
                        <FlatButton onTouchTap={addExamination} style={consts.TagBarButtonStyle} label="New examination" />
                    </ToolbarGroup>
                </Toolbar>
            </Header>
            {this.props.isLoading ? <LoadingScreen /> : this.props.examinations.size > 0 ?
                <List>
                    <Subheader>Examinations</Subheader>
                    {this.props.examinations.map(e =>
                        <ExaminationEntry
                            key={e.id}
                            examination={e}
                            protocolId={this.props.protocolId}
                            openExamination={this.props.openExamination} />
                    )}
                </List>
                : <NotFound message="No examinations were found" />
            }
        </div>;
    }

}

const mapProps = (state: AppState): ExaminationSelectorProps => ({
    protocolId: state.protocols.editing.id,
    isLoading: state.examinations.list.isLoading || state.protocols.editing.isLoading,
    examinations: state.examinations.list.loadedExaminations,
    filterText: state.examinations.list.filterText,
    filterErrorText: state.examinations.list.filterError,
    filter: state.examinations.list.filter
});

const mapActions = (dispatch: Dispatch<AppState>): ExaminationSelectorActions => ({
    refreshData: (protocolId, filter) => dispatch(loadExaminations(protocolId, filter)),
    openExamination: (protocolId, examId) => dispatch(editExamination(protocolId, examId)),
    newExamination: protocolId => dispatch(openNewExamination(protocolId)),
    changeFilter: (filterText, filter, errorText) => dispatch(changeFilter(filterText, filter, errorText)),
    recomputeProtocol: (protocolId: string) => dispatch(recomputeProtocol(protocolId))
});

export const ExaminationSelector = connect(mapProps, mapActions)(ExaminationSelectorControl);
