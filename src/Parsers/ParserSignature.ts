import { SignalData, AttributesCollection, TagValue } from "../Contract";

/**
 * Single signal detected and returned by parser
 */
export interface SignalAssignment {
    /**
     * Name of protocol signal, the signal is to be assigned to
     * (or empty string if signal is to be left for manual assignment).
     */
    signal: string;
    /**
     * Name of protocol stage, the signal is to be assigned to
     * (or empty string if signal is to be left for manual assignment).
    */
    stage: string;
    /**
     * Raw signal data (possibly mutli-channel).
     */
    data: SignalData;
    /**
     * Arbitrary signal description to be displayed in import GUI.
     * It should be unique for every returned signal.
     */
    description: string;
}

/**
 * Data returned by parser. First tuple element is collection of loaded tags.
 * Second tuple element is collection on loaded signals.
 * Signals can be automatically assigned to protocol location by setting
 * signalName and stageName properties. If left as empty string signal
 * will be left for manual assignment by user in GUI.
 */
export type ParserReturnType = [AttributesCollection<TagValue>, SignalAssignment[]];

/**
 * Signature of single parsing function. Function is invoked asynchronously
 * and has to resolve parsing by calling either resolve or reject functions.
 * Calling resolve will result in succesful import, and displaying returned result in GUI.
 * Calling reject will result in displaying stringified reason as an error message.
 * The first parameter is path to imported file. Parser is responsible for opening it.
 */
export type ParserSignature = (filename: string, resolve: (result: ParserReturnType) => void, reject: (reason?: any) => void) => void;

/**
 * Complete parser description object.
 */
export interface ParserRegisterEntry {
    /**
     * Name of parser to be displayed in GUI.
     */
    name: string;
    /**
     * Case-sensitive accepted extensions, i.e. ["dat", "DAT"].
     */
    extensions: string[];
    /**
     * Parsing function.
     */
    parser: ParserSignature;
}

export { AttributesCollection, TagValue };
