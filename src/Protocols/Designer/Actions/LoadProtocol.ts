import { remote } from "electron";

import {
    Action, ThunkAction,
    PromiseDataAction, PendingPromiseDataAction, FulfilledPromiseAction, RejectedPromiseAction,
} from "../../../Utils";
import { ServerClient, handleAndNavigateBack } from "../../../Client";

import { AppState } from "../../../State";
import { Protocol } from "../../../Contract";

interface LoadProtocolData {
    readonly id: string;
}

interface LoadProtocolAction extends PromiseDataAction<Protocol, LoadProtocolData> {
    type: "LoadProtocol";
}
interface LoadProtocolPendingAction extends PendingPromiseDataAction<Protocol, LoadProtocolData> {
    type: "LoadProtocol_PENDING";
}
interface LoadProtocolFulfilledAction extends FulfilledPromiseAction<Protocol> {
    type: "LoadProtocol_FULFILLED";
}
interface LoadProtocolRejectedAction extends RejectedPromiseAction<Protocol> {
    type: "LoadProtocol_REJECTED";
}

export type LoadProtocolResult = LoadProtocolPendingAction | LoadProtocolFulfilledAction | LoadProtocolRejectedAction;

export const isLoadProtocol =
    (a: Action<LoadProtocolAction>): a is LoadProtocolAction => a.type === "LoadProtocol";
export const isLoadProtocolPending =
    (a: Action<LoadProtocolPendingAction>): a is LoadProtocolPendingAction => a.type === "LoadProtocol_PENDING";
export const isLoadProtocolFulfilled =
    (a: Action<LoadProtocolFulfilledAction>): a is LoadProtocolFulfilledAction => a.type === "LoadProtocol_FULFILLED";
export const isLoadProtocolRejected =
    (a: Action<LoadProtocolRejectedAction>): a is LoadProtocolRejectedAction => a.type === "LoadProtocol_REJECTED";


export function loadProtocol(protoId: string): ThunkAction<AppState> {
    return dispatch => dispatch({
        type: "LoadProtocol",
        payload: {
            promise: ServerClient.loadProtocol(protoId),
            data: { id: protoId }
        }
    }).catch(handleAndNavigateBack);
}
