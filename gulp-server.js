'use strict';

const del = require('del');
const childProcess = require('child_process');

const gulp = require('gulp');
const typescript = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const merge = require('merge-stream');
const watch = require('gulp-watch');
const tslint = require('gulp-tslint');

const { copyOptional } = require('./gulp-scripts.js');

const paths = {
    tsconfig: './server/tsconfig.json',
    typescript: ['./server/**/*.ts', '!./server/node_modules/**/*.ts', '!./server/**/*.d.ts'],

    nodeModules: './server/node_modules/**',

    additionalFiles: [
        './server/package.json',
        './server/Dockerfile',
        './server/.dockerignore',
        './server/docker-compose.yml',
        './server/docker-compose.prod.yml',
        './server/docker-compose.dev.yml'
    ],

    dist: './dist/server',
    nodeDist: './dist/server/node_modules'
};

const tsProject = typescript.createProject(paths.tsconfig);

gulp.task('clean:server', () => del(paths.dist));
gulp.task('clean:server:all', () => del([
    paths.dist,
    paths.nodeModules
]));

gulp.task('tslint:server', () => {
    return gulp.src(paths.typescript)
        .pipe(tslint())
        .pipe(tslint.report());
});

gulp.task('typescript:server', ['tslint:server'], () =>
    gulp.src(paths.typescript)
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .js.pipe(sourcemaps.write('.', {
            includeContent: false,
            destPath: paths.dist,
            sourceRoot: '../server'
        }))
        .pipe(gulp.dest(paths.dist))
);

gulp.task('copy-static:server', () => merge(
    copyOptional(paths.nodeModules, paths.nodeDist),
    gulp.src(paths.additionalFiles).pipe(gulp.dest(paths.dist))
));

gulp.task('build:server', ['typescript:server', 'copy-static:server']);

function spawnApp(closeCallback) {
    return childProcess.spawn('node', ['--inspect=9229', '--debug', '.'], {
        stdio: 'inherit',
        cwd: paths.dist
    }).on('close', closeCallback);
}

function watchApp(closeCallback) {
    let app = spawnApp(closeCallback);

    const restartApp = () => {
        app.removeAllListeners('close').kill();
        app = spawnApp(closeCallback);
    };

    watch(paths.typescript, () => gulp.start('typescript:server', restartApp))

    return () => app.removeAllListeners('close').kill();
}

gulp.task('start:server', ['build:server'], () => spawnApp(() => process.exit()));
gulp.task('watch:server', ['build:server'], () => watchApp(() => process.exit()));

module.exports = { spawnApp, watchApp };
