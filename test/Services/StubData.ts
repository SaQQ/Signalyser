import * as db from "../../server/Db/DbClient";
import { IProtocolsClient } from "../../server/Db/ProtocolsClient";
import { IExaminationsClient } from "../../server/Db/ExaminationsClient";
import { ISignalsClient } from "../../server/Db/SignalsClient";

import { ProtocolUpdate, Protocol } from "../../server/Contract";

export class StubProtocolsClient implements IProtocolsClient {
    public updateError = false;
    public id: string;
    public proto: Protocol;

    async update(id: string, proto: Protocol): Promise<db.UpdateResult> {
        this.id = id;
        this.proto = proto;

        if (this.updateError) {
            return db.internalError();
        } else {
            return { type: "ObjectUpdated", id };
        }
    }

    reset() {
        this.updateError = false;
        this.id = <any>undefined;
        this.proto = <any>undefined;
    }

    all(): never { throw new Error("Not supported"); }
    add(): never { throw new Error("Not supported"); }
    find(): never { throw new Error("Not supported"); }
}

export class StubExaminationsClient implements IExaminationsClient {

    public updateError = false;

    public protoId: string;
    public tags: db.NamedUpdate[];
    public params: db.NamedUpdate[];

    async rename(protoId: string, tags: db.NamedUpdate[], params: db.NamedUpdate[]) {
        this.protoId = protoId;
        this.tags = tags;
        this.params = params;

        if (this.updateError) {
            return db.internalError();
        } else {
            return db.collectionUpdated();
        }
    }

    reset() {
        this.updateError = false;
        this.protoId = <any>undefined;
        this.tags = <any>undefined;
        this.params = <any>undefined;
    }

    all(): never { throw new Error("Not supported"); }
    add(): never { throw new Error("Not supported"); }
    find(): never { throw new Error("Not supported"); }
    update(): never { throw new Error("Not supported"); }
    setParameter(): never { throw new Error("Not supported"); }
}

export class StubSignalsClient implements ISignalsClient {
    public updateError = false;
    public protoId: string;
    public stages: db.NamedUpdate[];
    public signals: db.NamedUpdate[];

    async rename(protoId: string, stages: db.NamedUpdate[], signals: db.NamedUpdate[]) {
        this.protoId = protoId;
        this.stages = stages;
        this.signals = signals;

        if (this.updateError) {
            return db.internalError();
        } else {
            return db.collectionUpdated();
        }
    }

    reset() {
        this.updateError = false;
        this.protoId = <any>undefined;
        this.stages = <any>undefined;
        this.signals = <any>undefined;
    }

    list(): never { throw new Error("Not supported"); }
    download(): never { throw new Error("Not supported"); }
    upload(): never { throw new Error("Not supported"); }
}
