# Signalyzer
-----------------------

Signalyzer is a client-server system for biological signals storage and automated analysis.

Both the client and the server use NodeJS as runtime. Client side is a standalone electron application.

After downloading sources install runtime and additional packages usinging:

    (source_root)/ $ npm install

## Server

The server requires connection to MongoDB for data storage. The default connection string is *"mongodb://localhost:27017/signalyser"* and can be overridden by environment variable DATABASE.
For testing purposes the simplest solution is to run local mongodb daemon:

    mongod --dbpath=<some_empty_folder>

After installation server can be run using gulp:

    (source_root)/ $ ./node_modules/.bin/gulp start:server

By default, the server listens on port 5000. This can be overridden by setting environment variable PORT.

## Client

After installation client can be run using gulp:

    (source_root)/ $ ./node_modules/.bin/gulp start:app

## Test data

Example files to load are present in directory *(source_root)/data*