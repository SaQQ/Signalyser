import { should } from "../Startup";

import { FullExamination, ParameterDescriptor } from "../../server/Contract";

import { CodeError, CodeRunner } from "../../server/Computations/CodeRunner";
import * as config from "../../server/Config";

import * as stub from "./StubData";

describe("Computations/ComputationRunner#compute", function () {
    let runner = new CodeRunner();

    it("throws when the code is invalid", function () {
        should.throw(() => run("this code is invalid"), CodeError);
    });

    it("throws when the code does exports something different than function", function () {
        should.throw(() => run("exports.default = 5"), CodeError);
    });

    it("correctly executes function exported as default", function () {
        const result = run("exports.default = () => 5");
        result.should.equal(5);
    });

    it("correctly executes function exported as calculate", function () {
        const result = run("exports.calculate = () => 5");
        result.should.equal(5);
    });

    it("throws when the code exports both default and `calculate` functions", function () {
        should.throw(() => run("exports.default = () => 5; exports.calculate = () => 6;"), CodeError);
    });

    it("throws when the code exports something additional to default", function () {
        should.throw(() => run("exports.default = () => 5; exports.other = () => 6;"), CodeError);
    });

    it("throws when the code exports something additional to calculate", function () {
        should.throw(() => run("exports.default = () => 5; exports.other = () => 6;"), CodeError);
    });

    it("ignores exported `__esModule` if it is boolean", function () {
        should.not.throw(() => run("exports.__esModule=true; exports.default = () => 6;"));
    });

    it("throws when `__esModule` is not a boolean", function () {
        should.throw(() => run("exports.__esModule=\"alamakota\"; exports.default = () => 6;"));
    });

    ["global", "window", "require", "console"].forEach(fn => {
        it("blocks access to " + fn, function () {
            should.throw(() => run("exports.default = () => " + fn), CodeError);
        });
    });

    it("passes copy of exam as first parameter (without precomputed params)", function () {
        const [examination, param] = makeSample("exports.default = e => e");
        const {params, ...rest} = examination;
        const result = runner.run(examination, param);
        result.should.deep.equal({ ...rest, params: {} });
    });

    it("passes examination as the only argument", function () {
        run("exports.default = function () { return arguments.length; }").should.be.equal(1);
    });

    it("does not require to use parentheses around arrow function with body", function () {
        should.not.throw(() => run("exports.default = () => { return 5; }"));
    });

    it("does not require to use parentheses around normal function", function () {
        should.not.throw(() => run("exports.default = function () { return 5; }"));
    });

    it("ignores `formula` property, even if it is invalid", function () {
        should.not.throw(() => run("exports.default = () => 5;", "some invalid code that should not be parsed"));
    });

    it.skip("times out if the compilation/first execution takes longer than " + config.RunnerTimeout + " ms", function () {
        let timeout = config.RunnerTimeout * 2;
        should.throw(() => run("let s=Date.now();for(;Date.now()-s<" + timeout + ";);exports.default=()=>5;"), CodeError);
    });

    it.skip("throws if calculation takes more than " + config.CalculationTimeout + " ms", function () {
        let timeout = config.CalculationTimeout * 2;
        should.throw(() => run("exports.default=()=>{let s=Date.now();for(;Date.now()-s<" + timeout + ";);}"), CodeError);
    });

    function makeSample(code: string, formula?: string): [FullExamination, ParameterDescriptor] {
        const param = {
            name: "param",
            formula: formula || "",
            compiledFormula: code
        };
        return [stub.makeFullExamination(param), param];
    }

    function run(code: string, formula?: string) {
        const [exam, param] = makeSample(code, formula);
        return runner.run(exam, param);
    }
});
