'use strict';

const del = require('del');
const childProcess = require('child_process');

const gulp = require('gulp');
const typescript = require('gulp-typescript');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const mocha = require('gulp-spawn-mocha');

const { copyOptional } = require('./gulp-scripts.js');

const paths = {
    tsconfig: './test/tsconfig.json',
    typescript: ['./test/**/*.ts', '!./test/node_modules/**/*.ts', '!./test/**/*.d.ts'],
    serverWatch: ['./server/**/*.ts', '!./server/node_modules/**/*.ts', '!./server/**/*.d.ts'],

    nodeModules: './test/node_modules/**',

    dist: './dist/test',
    nodeDist: './dist/test/node_modules',
    distTest: ['./dist/test/**/*Tests.js', '!./dist/test/node_modules/**/*.js'],

    mochaBin: './test/node_modules/.bin/mocha'
};

const tsProject = typescript.createProject(paths.tsconfig);

gulp.task('clean:test', () => del(paths.dist));
gulp.task('clean:test:all', () => del([
    paths.dist,
    paths.nodeModules
]));

gulp.task('typescript:test', () =>
    // This is shit, but otherwise it will grumble about missing modules...
    gulp.src(paths.typescript.concat(paths.serverWatch))
        .pipe(sourcemaps.init())
        .pipe(tsProject())
        .js.pipe(sourcemaps.write('.', {
            includeContent: false,
            destPath: paths.dist,
            sourceRoot: '../test'
        }))
        .pipe(gulp.dest(paths.dist))
);

gulp.task('copy-static:test', () => copyOptional(paths.nodeModules, paths.nodeDist));

gulp.task('build:test', ['typescript:test', 'copy-static:test']);

gulp.task('test', ['build'], () =>
    gulp.src(paths.distTest).pipe(mocha({ bin: paths.mochaBin }))
);

gulp.task('test:watch', ['build'], () => {
    let mocha = childProcess.spawn(paths.mochaBin, ['--inspect=9229', '--debug', '--reporter=nyan', '--watch', paths.dist + "/**/*Tests.js"], {
        stdio: 'inherit'
    });

    watch(paths.typescript, () => gulp.start('typescript:test'));
    watch(paths.serverWatch, () => gulp.start('typescript:server'));

    process.on('SIGINT', () => { mocha.kill(); process.exit(0); });
});
