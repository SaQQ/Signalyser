"use strict";

let base = require("../promise/index");

module.exports = class extends base {
    constructor(args, options) {
        super(args, options);

        this.option("data", { type: String, desc: "The type of data that the promise returns", defaults: "any" });
        this.option("define-data", { type: Boolean, desc: "Define the resulting type as interface", defaults: false });
    }

    _getTplArgs() {
        return Object.assign(super._getTplArgs(), {
            data: this.options.data,
            defineData: this.options.defineData
        });
    }

    writing() {
        super.writing();
    }
}
