import { remote } from "electron";
import {
    Action, ThunkAction,
    PromiseAction, PendingPromiseAction, FulfilledPromiseAction, RejectedPromiseAction,
} from "../../../Utils";
import { ServerClient, handleAndNavigateBack } from "../../../Client";

import { AppState } from "../../../State";
import { RealSignal } from "../State";
import { Protocol, Examination } from "../../../Contract";

interface LoadExaminationAction extends PromiseAction<[Protocol, Examination, RealSignal[]]> {
    type: "LoadExamination";
}
interface LoadExaminationPendingAction extends PendingPromiseAction<[Protocol, Examination, RealSignal[]]> {
    type: "LoadExamination_PENDING";
}
interface LoadExaminationFulfilledAction extends FulfilledPromiseAction<[Protocol, Examination, RealSignal[]]> {
    type: "LoadExamination_FULFILLED";
}
interface LoadExaminationRejectedAction extends RejectedPromiseAction<[Protocol, Examination, RealSignal[]]> {
    type: "LoadExamination_REJECTED";
}

export type LoadExaminationResult = LoadExaminationPendingAction | LoadExaminationFulfilledAction | LoadExaminationRejectedAction;

export const isLoadExamination =
    (a: Action<LoadExaminationAction>): a is LoadExaminationAction => a.type === "LoadExamination";
export const isLoadExaminationPending =
    (a: Action<LoadExaminationPendingAction>): a is LoadExaminationPendingAction => a.type === "LoadExamination_PENDING";
export const isLoadExaminationFulfilled =
    (a: Action<LoadExaminationFulfilledAction>): a is LoadExaminationFulfilledAction => a.type === "LoadExamination_FULFILLED";
export const isLoadExaminationRejected =
    (a: Action<LoadExaminationRejectedAction>): a is LoadExaminationRejectedAction => a.type === "LoadExamination_REJECTED";

async function load(protoId: string, examId: string) {
    let protocol = await ServerClient.loadProtocol(protoId);
    let exam = await ServerClient.loadExamination(protoId, examId);
    let signals: RealSignal[] = [];
    for (let i = 0; i < exam.signals.length; ++i) {
        let sig = exam.signals[i];
        let data = await ServerClient.downloadSignal(protoId, examId, sig.stage, sig.signal);
        signals.push({ ...sig, data });
    }
    return [protocol, exam, signals];
}

export function loadExamination(protoId: string, examId: string): ThunkAction<AppState> {
    return dispatch => dispatch({
        type: "LoadExamination",
        payload: load(protoId, examId)
    }).catch(handleAndNavigateBack(dispatch));
}
