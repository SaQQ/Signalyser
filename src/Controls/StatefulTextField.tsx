/// <reference path="../node_modules/@types/material-ui/index.d.ts" />

import * as React from "react";
import TextField from "material-ui/TextField";

export interface StatefulTextFieldProps extends __MaterialUI.TextFieldProps {
    readonly onValueChange: (newVal: string | number) => void;
}

interface StatefulTextFieldState {
    readonly currVal: string | number;
}

export class StatefulTextField extends React.Component<StatefulTextFieldProps, StatefulTextFieldState> {

    constructor(props: StatefulTextFieldProps) {
        super(props);

        this.state = { currVal: props.value || "" };
    }

    render() {
        let { onChange, onValueChange, onBlur, onKeyDown,
            value, ...rest } = this.props;
        return <TextField
            {...rest}
            value={this.state.currVal}
            onKeyDown={this.onKeyDown}
            onChange={this.onChange} onBlur={this.onBlur} />;
    }

    private onChange = (e: React.FormEvent<{}>, newValue: string) => {
        if (this.props.onChange !== undefined) {
            this.props.onChange(e, newValue);
        }
        this.setState({ currVal: newValue });
    }

    private onBlur = (e: React.FocusEvent<{}>) => {
        if (this.props.onBlur !== undefined) {
            this.props.onBlur(e);
        }
        this.props.onValueChange(this.state.currVal);
    }

    private onKeyDown = (e: React.KeyboardEvent<{}>) => {
        if (this.props.onKeyDown !== undefined) {
            this.props.onKeyDown(e);
        }
        if (e.keyCode === 13) {
            this.props.onValueChange(this.state.currVal);
        }
    }

}
