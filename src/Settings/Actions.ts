import { Action, ReduxAction, ThunkAction } from "../Utils/Redux";
import { Settings } from "./Settings";

import { openProtocolsList } from "../Protocols/Navigation";

import { AppState } from "../State";

export interface OpenSettingsAction extends ReduxAction {
    type: "OpenSettings";
}

export interface SaveSettingsAction extends ReduxAction {
    type: "SaveSettings";
    serverAddress: string;
}

export interface DiscardSettingsAction extends ReduxAction {
    type: "DiscardSettings";
}

export type SettingsAction = OpenSettingsAction | SaveSettingsAction | DiscardSettingsAction;

export const isOpenSettings = (a: Action<OpenSettingsAction>): a is OpenSettingsAction => a.type === "OpenSettings";
export const isSaveSettings = (a: Action<SaveSettingsAction>): a is SaveSettingsAction => a.type === "SaveSettings";
export const isDiscardSettings = (a: Action<DiscardSettingsAction>): a is DiscardSettingsAction => a.type === "DiscardSettings";

export const openSettings = (): OpenSettingsAction => ({ type: "OpenSettings" });
export const discardSettings = (): DiscardSettingsAction => ({ type: "DiscardSettings" });

export function saveSettings(serverAddress: string): ThunkAction<AppState> {
    return dispatch => {
        Settings.update({ serverAddress });

        dispatch({ type: "SaveSettings", serverAddress });
        dispatch(openProtocolsList(true));
    };
}
