import * as React from "react";
import * as Redux from "redux";

import { Router, withRouter } from "react-router";
import { Location } from "history";

import AppBar from "material-ui/AppBar";
import IconButton from "material-ui/IconButton";
import { Toolbar } from "material-ui/Toolbar";
import NavigationArrowBack from "material-ui/svg-icons/navigation/arrow-back";

import { AppState } from "../State";
import { openSettings } from "../Settings";

export interface HeaderProps {
    title: string;
    children?: React.ReactElement<any>;
}

function isToolbar(element: React.ReactElement<any>): element is React.DOMElement<any, Toolbar> {
    let asAny = element as any;
    return asAny.type !== null && asAny.type.name === "Toolbar";
}

function fixToolbar(element?: React.ReactElement<any>) {
    if (element !== undefined && isToolbar(element)) {
        let newProps = {
            style:
            {
                height: 36,
                backgroundColor: "transparent"
            }
        };
        return React.cloneElement(element, newProps);
    }
    return element;
}

function fixStyleRight(element?: React.ReactElement<any>): React.CSSProperties {
    if (element !== undefined && isToolbar(element)) {
        return { marginTop: 14 };
    }
    return {};
}

export class Header extends React.Component<HeaderProps, {}> {
    static contextTypes = {
        router: React.PropTypes.object,
        store: React.PropTypes.object
    };

    context: {
        router: ReactRouter.InjectedRouter & ReactRouter.RouterState;
        store: Redux.Store<AppState>;
    };

    render() {
        let title = this.props.title || "Signalyser";
        let isRoot = this.context.router.location.pathname === "/";
        let backButton = isRoot ? undefined : <IconButton><NavigationArrowBack /></IconButton>;

        let content = fixToolbar(this.props.children);

        let leftButtonClick =
            isRoot ? () => this.context.store.dispatch(openSettings()) :
            () => this.context.router.goBack();

        return <AppBar
            zDepth={2}
            title={title}
            iconElementLeft={backButton}
            onLeftIconButtonTouchTap={leftButtonClick}
            iconElementRight={content}
            iconStyleRight={fixStyleRight(this.props.children)} />;
    }

}
