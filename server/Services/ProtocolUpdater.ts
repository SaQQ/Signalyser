import * as winston from "winston";

import * as db from "../Db/DbClient";

import { IProtocolsClient } from "../Db/ProtocolsClient";
import { IExaminationsClient } from "../Db/ExaminationsClient";
import { ISignalsClient } from "../Db/SignalsClient";

import { Protocol, ProtocolUpdate, UpdateNamed } from "../Contract";

export interface IProtocolUpdater {
    update(protoId: string, proto: ProtocolUpdate): Promise<db.UpdateResult>;
}

export class ProtocolUpdater implements IProtocolUpdater {

    constructor(
        private readonly protocols: IProtocolsClient,
        private readonly examinations: IExaminationsClient,
        private readonly signals: ISignalsClient) {
    }

    async update(protoId: string, proto: ProtocolUpdate): Promise<db.UpdateResult> {
        const updated = this.buildNewProtocol(proto);
        const updateResult = await this.protocols.update(protoId, updated);
        if (updateResult.type === "ObjectUpdated") {

            const tags = this.buildRenameAction(proto.tags);
            const params = this.buildRenameAction(proto.params);

            let result = await this.examinations.rename(protoId, tags, params);
            if (result.type === "DbActionError") {
                winston.warn("Cannot update tags/params of examinations of protocol %s, error: %d, ignoring", protoId, result.error);
            } else {
                winston.info("%d tags and %d params updated in protocol %s", tags.length, params.length, protoId);
            }

            const signals = this.buildRenameAction(proto.signals);
            const stages = this.buildRenameAction(proto.stages);

            result = await this.signals.rename(protoId, stages, signals);
            if (result.type === "DbActionError") {
                winston.warn("Cannot update stage/signal of signals of protocol %s, error: %d, ignoring", protoId, result.error);
            } else {
                winston.info("%d stages and %d signals updated in protocol %s", stages.length, signals.length, protoId);
            }

        } else {
            winston.error("Cannot update protocol %s, error: %d, ignoring rename actions", protoId, updateResult.error);
        }
        return updateResult;
    }

    private buildRenameAction(changes: ({ name: string } & UpdateNamed)[]): db.NamedUpdate[] {
        return changes
            .filter(c => c.newName !== c.name && c.newName !== "")
            .map(n => ({ oldName: n.name, newName: n.newName }));
    }

    private buildNewProtocol(proto: ProtocolUpdate): Protocol {
        return {
            id: proto.id,
            name: proto.name,
            signals: proto.signals.map(s => ({ name: s.newName })),
            stages: proto.stages.map(s => ({ name: s.newName })),
            tags: proto.tags.map(t => ({ name: t.newName })),
            params: proto.params.map(p => ({ name: p.newName, formula: p.formula, compiledFormula: p.compiledFormula }))
        };
    }

}
