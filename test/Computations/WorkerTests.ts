import { should } from "../Startup";

import { ErrorType } from "../../server/Db/DbClient";
import { CodeError } from "../../server/Computations/CodeRunner";
import { Worker } from "../../server/Computations/Worker";

import * as stub from "./StubData";

describe("Computations/Worker#compute", function () {
    const runner = new stub.ComputationsRunner();
    const reader = new stub.ExaminationReader();
    const worker = new Worker(runner, reader);

    beforeEach(function () {
        runner.reset();
        reader.reset();
    });

    it("calls reader to get the examination", function () {
        return run().should.eventually.be.fulfilled
            .then(r => {
                reader.called.should.be.true;
                reader.protoId.should.be.equal(stub.protoId);
                reader.examId.should.be.equal(stub.examId);
            });
    });

    it("passes retrieved examination to the runner", function () {
        return run().should.eventually.be.fulfilled
            .then(r => {
                runner.called.should.be.true;
                runner.examination.should.be.equal(stub.readExamination);
            });
    });

    it("returns the computed result, along with all properties, as job finished", function () {
        return run().should.eventually.be.deep.equal(stub.successfulJobResult);
    });

    it("returns error when it cannot get examination from the reader", function () {
        return runReadError(new Error("Sample error")).should.eventually.be.fulfilled
            .then((r: any) => r.should.have.property("type", "Errored"));
    });

    it("returns error when it cannot locate the specified parameter", function () {
        return runUnkownParam().should.eventually.be.fulfilled
            .then((r: any) => r.should.have.property("type", "Errored"));
    });

    it("passes found parameter to the runner", function () {
        return run().should.eventually.be.fulfilled
            .then((r: any) => {
                runner.called.should.be.true;
                runner.param.should.be.equal(stub.param);
            });
    });

    it("returns error when computation fails", function () {
        return runComputeError(new CodeError("Some error")).should.eventually.be.fulfilled
            .then((r: any) => r.should.have.property("type", "Errored"));
    });

    function run() {
        reader.result = stub.readExamination;
        runner.result = stub.successfulJobResult.data;
        return worker.compute(stub.protoId, stub.examId, stub.paramName);
    }

    function runReadError(error: Error) {
        reader.error = error;
        runner.result = stub.successfulJobResult.data;
        return worker.compute(stub.protoId, stub.examId, stub.paramName);
    }

    function runUnkownParam() {
        const exam = stub.makeReadExamination({ name: "unknown-param", formula: "", compiledFormula: "" });
        reader.result = exam;
        runner.result = stub.successfulJobResult.data;
        return worker.compute(stub.protoId, stub.examId, stub.paramName);
    }

    function runComputeError(error: CodeError) {
        reader.result = stub.readExamination;
        runner.error = error;
        return worker.compute(stub.protoId, stub.examId, stub.paramName);
    }
});
