import { combineReducersInOrder, idReducer } from "../../../Utils";

import { ExaminationsListState, emptyExaminationsListState } from "../State";
import { loadExaminationsReducer } from "./LoadExaminationsReducer";
import { filterReducer } from "./FilterReducer";
import { recomputeProtocolReducer } from "./RecomputeReducers";

const initialState = idReducer(emptyExaminationsListState);

export const examinationsListReducer = combineReducersInOrder(
    initialState,
    filterReducer,
    loadExaminationsReducer,
    recomputeProtocolReducer,
);
