import { Action } from "../../../Utils";
import { ExaminationsList, ExaminationsListState } from "../State";

import {
    RecomputeProtocolAction, RecomputeProtocolResult,
    isRecomputeProtocol, isRecomputeProtocolPending, isRecomputeProtocolFulfilled, isRecomputeProtocolRejected
} from "./Recompute";

export function recomputeProtocolReducer(state: ExaminationsListState, action: Action<RecomputeProtocolAction>): ExaminationsListState {
    if (isRecomputeProtocol(action)) {
        return { ...state, isLoading: true };
    } if (isRecomputeProtocolPending(action)) {
        return { ...state, isLoading: true };
    } else if (isRecomputeProtocolFulfilled(action)) {
        return { ...state, isLoading: false };
    } else if (isRecomputeProtocolRejected(action)) {
        return { ...state, isLoading: false };
    }
    return state;
}
