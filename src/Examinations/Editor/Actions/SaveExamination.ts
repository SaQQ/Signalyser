import { remote } from "electron";
import {
    Action, ThunkAction,
    PromiseAction, PendingPromiseAction, FulfilledPromiseAction, RejectedPromiseAction,
} from "../../../Utils";
import { ServerClient, handleClientError } from "../../../Client";

import { AppState } from "../../../State";
import { EditingExaminationState } from "../State";
import { ExaminationWithTags, CreatedOrUpdated } from "../../../Contract";

interface SaveExaminationAction extends PromiseAction<CreatedOrUpdated> {
    type: "SaveExamination";
}
interface SaveExaminationPendingAction extends PendingPromiseAction<CreatedOrUpdated> {
    type: "SaveExamination_PENDING";
}
interface SaveExaminationFulfilledAction extends FulfilledPromiseAction<CreatedOrUpdated> {
    type: "SaveExamination_FULFILLED";
}
interface SaveExaminationRejectedAction extends RejectedPromiseAction<CreatedOrUpdated> {
    type: "SaveExamination_REJECTED";
}
export const isSaveExamination =
    (a: Action<SaveExaminationAction>): a is SaveExaminationAction => a.type === "SaveExamination";
export const isSaveExaminationPending =
    (a: Action<SaveExaminationPendingAction>): a is SaveExaminationPendingAction => a.type === "SaveExamination_PENDING";
export const isSaveExaminationFulfilled =
    (a: Action<SaveExaminationFulfilledAction>): a is SaveExaminationFulfilledAction => a.type === "SaveExamination_FULFILLED";
export const isSaveExaminationRejected =
    (a: Action<SaveExaminationRejectedAction>): a is SaveExaminationRejectedAction => a.type === "SaveExamination_REJECTED";

export type SaveExaminationResult = SaveExaminationPendingAction | SaveExaminationFulfilledAction | SaveExaminationRejectedAction;

async function uploadSignals(state: EditingExaminationState, examId: string) {
    const protoId = state.protocol.id;

    for (let i = 0; i < state.signals.size; i++) {
        const sig = state.signals.get(i);
        await ServerClient.uploadSignal(protoId, examId, sig.stage, sig.signal, sig.data);
    }
}

async function uploadExamination(state: EditingExaminationState, exam: ExaminationWithTags) {
    let result: CreatedOrUpdated;
    if (state.isNew) {
        result = await ServerClient.saveExamination(state.protocol.id, exam);
    } else {
        result = await ServerClient.updateExamination(state.protocol.id, exam);
    }
    await uploadSignals(state, result.id);
    await ServerClient.recomputeExamination(state.protocol.id, result.id);
    return result;
}

export function saveExamination(): ThunkAction<AppState> {
    return (dispatch, getState) => {
        let state = getState().examinations.editing;
        let exam = {
            id: state.id,
            tags: state.tags
        };
        // TODO: distinguish between exam/signal upload
        dispatch({
            type: "SaveExamination",
            payload: uploadExamination(state, exam)
        }).catch(handleClientError);
    };
}

