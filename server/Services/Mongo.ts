import * as db from "../Db/Mongo";
import { ProtocolUpdater } from "./ProtocolUpdater";

export const Updater = new ProtocolUpdater(db.Protocols, db.Examinations, db.Signals);
