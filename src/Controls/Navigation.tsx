import * as React from "react";
import * as Redux from "redux";

import * as History from "history";
import { push } from "react-router-redux";
import { ThunkAction, ReduxAction } from "../Utils";

import { AppState } from "../State";

type NavigationAction = ReduxAction | ThunkAction<AppState>;

export interface ActionElementProps {
    action: () => NavigationAction;
}

export interface NavigateElementProps {
    to: string | History.LocationDescriptor;
    action: () => NavigationAction;
}

export abstract class ActionElement<P extends ActionElementProps> extends React.Component<P, {}> {

    static contextTypes = {
        store: React.PropTypes.object
    };

    context: {
        store: Redux.Store<AppState>;
    };

    abstract render(): JSX.Element | null;

    protected doAction = () => {
        let action: any = this.props.action();
        this.context.store.dispatch(action);
    }

}

export class LinkButton extends ActionElement<ActionElementProps> {

    render() {
        return <button onClick={this.doAction} className="positive">{this.props.children}</button>;
    }

}
