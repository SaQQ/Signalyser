import * as immutable from "immutable";

import { Action } from "../../../Utils";
import { EditingExaminationState, RealSignal } from "../State";
import {
    NewExaminationResult,
    isNewExaminationPending, isNewExaminationFulfilled, isNewExaminationRejected
} from "./NewExamination";

export function newExaminationReducer(state: EditingExaminationState, action: Action<NewExaminationResult>): EditingExaminationState {
    if (isNewExaminationPending(action)) {
        return { ...state, isLoading: true };
    } else if (isNewExaminationFulfilled(action)) {
        return {
            ...state,
            isLoading: false,
            isNew: true,

            id: "",
            protocol: action.payload,
            tags: {},
            params: {},
            signals: immutable.List<RealSignal>()
        };
    } else if (isNewExaminationRejected(action)) {
        return { ...state, isLoading: false };
    }
    return state;
}
