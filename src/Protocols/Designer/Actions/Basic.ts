import { Action, ReduxAction } from "../../../Utils";

export interface NewProtocolAction extends ReduxAction {
    type: "NewProtocol";
}

export interface ChangeNameAction extends ReduxAction {
    type: "ChangeName";
    readonly newName: string;
}

export const isNewProtocol = (a: Action<NewProtocolAction>): a is NewProtocolAction => a.type === "NewProtocol";
export const isChangeName = (a: Action<ChangeNameAction>): a is ChangeNameAction => a.type === "ChangeName";

export const newProtocol = (): NewProtocolAction => ({ type: "NewProtocol" });
export const changeName = (newName: string): ChangeNameAction => ({ type: "ChangeName", newName });
