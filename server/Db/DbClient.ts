import { EventEmitter } from "events";
import * as mongo from "mongodb";
import * as winston from "winston";

import * as config from "../Config";

export enum ErrorType {
    NotFound,
    InternalError
}

export interface DbActionError {
    type: "DbActionError";
    error: ErrorType;
}

export interface ObjectCreated {
    type: "ObjectCreated";
    id: string;
}

export interface ObjectUpdated {
    type: "ObjectUpdated";
    id: string;
}

export interface CollectionUpdated {
    type: "CollectionUpdated";
}

export interface ObjectRead<T> {
    type: "ObjectRead";
    result: T;
}

export interface NamedUpdate {
    oldName: string;
    newName: string;
}

export function error(error: ErrorType): DbActionError {
    return { type: "DbActionError", error };
}

export function notFound(): DbActionError {
    return error(ErrorType.NotFound);
}

export function internalError(): DbActionError {
    return error(ErrorType.InternalError);
}

export function read<T>(result: T): ObjectRead<T> {
    return { type: "ObjectRead", result };
}

export function created(id: mongo.InsertOneWriteOpResult | mongo.ObjectID): ObjectCreated {
    if (id instanceof mongo.ObjectID) {
        return { type: "ObjectCreated", id: id.toHexString() };
    } else {
        return { type: "ObjectCreated", id: id.insertedId.toHexString() };
    }
}

export function updated(id: mongo.ObjectID): ObjectUpdated {
    return { type: "ObjectUpdated", id: id.toHexString() };
}

export function collectionUpdated(): CollectionUpdated {
    return { type: "CollectionUpdated" };
}

export type CreateResult = DbActionError | ObjectCreated;
export type UpdateResult = DbActionError | ObjectUpdated;
export type ReadResult<T> = DbActionError | ObjectRead<T>;
export type CollectionUpdateResult = DbActionError | CollectionUpdated;

export interface IDbClient extends EventEmitter {
    connect(): Promise<void>;
    disconnect(): Promise<void>;

    createExaminationsCollection(protoId: string): Promise<void>;

    protocols: mongo.Collection;
    signals: mongo.GridFSBucket;
    signalFiles: mongo.Collection;
    examinations(id: string): mongo.Collection | null;

    on(event: "connected", listener: () => void): this;
}

const Protocols = "protocols";
const ExaminationsPrefix = "exams-";
const SignalsBucket = "signals";

export class DbClient extends EventEmitter implements IDbClient {
    private db: mongo.Db;
    private savedProtocols: mongo.Collection;
    private signalsBucket: mongo.GridFSBucket;
    private savedSignalFiles: mongo.Collection;

    async connect(): Promise<void> {
        winston.info("Connecting to MongoDB at %s", config.DatabaseConnectionString);
        this.db = await mongo.MongoClient.connect(config.DatabaseConnectionString);
        await this.prepareCollections();

        this.emit("connected");
    }

    async disconnect() {
        return this.db.close();
    }

    async createExaminationsCollection(protoId: string) {
        await this.db.createCollection(ExaminationsPrefix + protoId);
    }

    get protocols() {
        return this.savedProtocols;
    }

    get signals() {
        return this.signalsBucket;
    }

    get signalFiles() {
        return this.savedSignalFiles;
    }

    examinations(id: string) {
        return this.db.collection(ExaminationsPrefix + id);
    }

    private async prepareCollections() {
        this.savedProtocols = await this.db.createCollection(Protocols);
        this.signalsBucket = new mongo.GridFSBucket(this.db, {
            bucketName: SignalsBucket
        });

        this.savedSignalFiles = await this.db.createCollection(SignalsBucket + ".files");
        let index = await this.savedSignalFiles.createIndex({ "metadata.examId": 1 });
    }
}
