import * as React from "react";
import { Route } from "react-router";

import { NewProtocol, EditProtocol } from "./Navigation";

import { ProtocolSelector } from "./Selector/Component";
import { ProtocolDesigner } from "./Designer/Component";

export const Routes = [
    <Route key={NewProtocol} path={NewProtocol} component={ProtocolDesigner} />,
    <Route key={EditProtocol} path={EditProtocol} component={ProtocolDesigner} />
];
