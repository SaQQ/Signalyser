export interface AssignJobCommand {
    type: "AssignJobCommand";
    protoId: string;
    examId: string;
    paramName: string;
    refNumber: string;
}

export interface CancelJobCommand {
    type: "CancelJobCommand";
    refNumber: string;
}

export interface JobFinished {
    type: "JobFinished";
    refNumber: string;
    canceled: boolean;
}

export interface WorkerOnline {
    type: "WorkerOnline";
}
