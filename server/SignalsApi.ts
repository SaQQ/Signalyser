import * as stream from "stream";
import { Request, Response } from "express";

import { SignalMimeType, SignalData } from "./Contract";
import { translateCreate, translateResult } from "./Utils/Translators";

import * as validators from "./Validators";
import * as db from "./Db";

export async function upload(req: Request, res: Response) {
    let protoId = req.params.protocolId as string;
    let examId = req.params.examId as string;
    let stage = req.params.stage as string;
    let signal = req.params.signal as string;
    if (!req.is(SignalMimeType)) {
        return res.sendStatus(415);
    } else if (
        !validators.validateId(res, protoId) ||
        !validators.validateId(res, examId)) {
        return res;
    }

    res.status(200); // Set the status in advance (we may override it later)
    let result = await db.Signals.upload({ protoId, examId, stage, signal, data: req });
    return translateCreate(res, result);
}

export async function download(req: Request, res: Response) {
    let protoId = req.params.protocolId as string;
    let examId = req.params.examId as string;
    let stage = req.params.stage as string;
    let signal = req.params.signal as string;
    if (!req.accepts(SignalMimeType)) {
        return res.sendStatus(406);
    } else if (
        !validators.validateId(res, protoId) ||
        !validators.validateId(res, examId)) {
        return res;
    }
    let result = await db.Signals.download({ protoId, examId, stage, signal, output: res });
    return translateResult(res, result, true);
}
