import { ProtocolsState } from "./Protocols/State";
import { ExaminationsState } from "./Examinations/State";
import { SettingsState } from "./Settings/State";

export interface AppState {
    readonly protocols: ProtocolsState;
    readonly examinations: ExaminationsState;
    readonly settings: SettingsState;
}
