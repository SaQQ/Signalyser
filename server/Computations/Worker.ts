import * as winston from "winston";

import { IExaminationReader, ReadExamination } from "./ExaminationReader";
import { ICodeRunner } from "./CodeRunner";

export interface Errored {
    type: "Errored";
    message: string;
    innerError?: Error;
}

export interface Computed {
    type: "Computed";
    data: Object;
}

export interface IWorker {
    compute(protoId: string, examId: string, paramName: string): Promise<Errored | Computed>;
}

class WorkerError extends Error {
    constructor(message: string, public readonly innerError?: Error) {
        super(message);
    }
}

export class Worker implements IWorker {

    constructor(
        private readonly runner: ICodeRunner,
        private readonly reader: IExaminationReader) {
    }

    async compute(protoId: string, examId: string, paramName: string): Promise<Errored | Computed> {
        try {
            const examination = await this.readExamination(protoId, examId);
            const result = await this.computeResult(paramName, examination);

            winston.verbose("Computation of parameter %s for examination %s, protocol %s finished", paramName, examId, protoId);

            return { type: "Computed", data: result };
        } catch (e) {
            return {
                type: "Errored",
                message: e.message,
                innerError: e.innerError
            };
        }
    }

    private async readExamination(protoId: string, examId: string) {
        try {
            winston.debug("Getting examination %s, protocol %s", examId, protoId);
            return await this.reader.download(protoId, examId);
        } catch (e) {
            winston.warn("Cannot get examination %s, protocol %s", examId, protoId, e);
            throw new WorkerError("Cannot read the examination " + examId + " of protocol " + protoId, e);
        }
    }

    private async computeResult(paramName: string, examination: ReadExamination) {
        const param = examination.protocol.params.find(p => p.name === paramName);
        if (param === undefined) {
            winston.warn("Cannot locate parameter %s in protocol %s", paramName, examination.protocol.id);
            throw new WorkerError(`Cannot locate parameter ${paramName} in the protocol ${examination.protocol.id}`);
        } else {
            try {
                return await this.runner.run(examination, param);
            } catch (e) {
                winston.warn("Cannot compute parameter %s for examination %s, protocol %s", paramName, examination.id, examination.protocol.id, e);
                throw new WorkerError("Cannot compute the value", e);
            }
        }
    }

}
