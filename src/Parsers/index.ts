import { List } from "immutable";

import { ParserRegisterEntry } from "./ParserSignature";

/**
 * Import new parsers here VVV
 */
import HAParser from "./HAParser";
import FinapressParser from "./FinapressParser";
import WinCPPRSParser from "./WinCPPRSParser";

/**
 * Parser registration array.
 * Add entry when creating new parser.
 */
const parsers: ParserRegisterEntry[] = [
    { extensions: ["dat"], name: "HA File Parser", parser: HAParser },
    { extensions: ["csv"], name: "Finapress parser", parser: FinapressParser },
    { extensions: ["dat"], name: "WinCPPRS parser", parser: WinCPPRSParser }
];

export const AvalaibleParsers = List<ParserRegisterEntry>(parsers);

export * from "./ParserSignature";
/**
 * Promise wrapper for parsers - do not modify
 */
export { default as Parse } from "./Parse";
