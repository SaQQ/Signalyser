import { Action, ReduxAction, } from "../../../Utils";

export interface OpenParserChooserAction extends ReduxAction {
    type: "OpenParserChooser";
}

export interface CloseParserChooserAction extends ReduxAction {
    type: "CloseParserChooser";
}

export type ParserChooserAction = OpenParserChooserAction | CloseParserChooserAction;

export const isOpenParserChooser = (a: Action<OpenParserChooserAction>): a is OpenParserChooserAction => a.type === "OpenParserChooser";
export const isCloseParserChooser = (a: Action<CloseParserChooserAction>): a is CloseParserChooserAction => a.type === "CloseParserChooser";

export const openParserChooser = (): OpenParserChooserAction => ({ type: "OpenParserChooser" });
export const closeParserChooser = (): CloseParserChooserAction => ({ type: "CloseParserChooser" });
