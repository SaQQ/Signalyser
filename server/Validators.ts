import { Response } from "express";

import * as is from "./Utils/Is";
import * as that from "./Utils/Is";
import * as t from "./Utils/Translators";
import {
    Protocol, ProtocolUpdate, UpdateNamed,
    SignalDescriptor, StageDescriptor, ExaminationWithTags, SignalData, TagValue
} from "./Contract";

interface Named { name: string; }
interface Identified { id: string; }
interface WithFormula { formula: string; compiledFormula: string; }
interface WithValue { value: string; }

const MongoIdRegex = /^[a-z0-9]+$/;
const UserIdRegex = /^[a-zA-Z0-9_]+$/;

const isMongoId = is.test<string>(l => l.length === 24 && MongoIdRegex.test(l), "The value must be valid MongoId", 100);
const isUserId = is.test<string>(l => UserIdRegex.test(l), "The value must be valid UserId", 101);

const isValidNamedObject = <T extends Named>(valid: is.BaseCheck<T>) =>
    valid.when(is.notNull)
        .andHas(d => d.name).that(is.notNull).andThat(is.notEmpty, is.fatal).andThat(isUserId);
const hasFormula = <T extends WithFormula>(valid: is.BaseCheck<T>) =>
    valid.whenHas(l => l.compiledFormula).that(is.notNull).andThat(is.notEmpty, is.fatal)
        .andHas(t => t.formula).that(is.notNull).andThat(is.notEmpty, is.fatal);
const hasValue = <T extends WithValue>(valid: is.BaseCheck<T>) =>
    valid.whenHas(l => l.value).that(is.notNull);

const isValidUpdate = <T extends Named & UpdateNamed>(valid: is.BaseCheck<T>) =>
    valid.when(is.notNull)
        .andHas(d => d.name).that(is.notNull).andThat(is.notEmpty, is.fatal).andThat(isUserId)
        .andHas(d => d.newName).that(is.notNull).andThat(is.notEmpty, is.fatal).andThat(isUserId);

export function validateFilter(res: Response, filter: Object) {
    return true;
}

export function validateId(res: Response, id: string) {
    let result = is.valid(id, "id")
        .when(is.notEmpty, is.fatal).and(isMongoId, is.fatal)
        .result;
    return t.checkValidity(res, result);
}

export function validateProtocol(res: Response, data: Protocol) {
    let result = is.valid(data)
        .when(is.notNull)
        .andHas(v => v.name).that(is.notNull)
        .andHas(v => v.stages).that(is.notNull).andThat(is.array).andThatFulfils(that.everyFulfils(isValidNamedObject))
        .andHas(v => v.signals).that(is.notNull).andThat(is.array).andThatFulfils(that.everyFulfils(isValidNamedObject))
        .andHas(v => v.tags).that(is.notNull).andThat(is.array).andThatFulfils(that.everyFulfils(isValidNamedObject))
        .andHas(v => v.params).that(is.notNull).andThat(is.array).andThatFulfils(that.everyFulfils(isValidNamedObject).and(hasFormula))
        .result;
    return t.checkValidity(res, result);
}

export function validateProtocolUpdate(res: Response, data: Protocol) {
    let result = is.valid(data)
        .andHas(v => v.name).that(is.notNull)
        .andHas(v => v.stages).that(is.notNull).andThat(is.array).andThatFulfils(that.everyFulfils(isValidUpdate))
        .andHas(v => v.signals).that(is.notNull).andThat(is.array).andThatFulfils(that.everyFulfils(isValidUpdate))
        .andHas(v => v.tags).that(is.notNull).andThat(is.array).andThatFulfils(that.everyFulfils(isValidUpdate))
        .andHas(v => v.params).that(is.notNull).andThat(is.array).andThatFulfils(that.everyFulfils(isValidUpdate).and(hasFormula))
        .result;

    return t.checkValidity(res, result);
}

export function validateExamination(res: Response, data: ExaminationWithTags) {
    let result = is.valid(data)
        .when(is.notNull)
        .andHas(v => v.tags).that(is.notNull)
        .result;
    return t.checkValidity(res, result);
}
