import { Writable } from "stream";
import * as winston from "winston";

import { IExaminationsClient } from "../Db/ExaminationsClient";
import { IProtocolsClient } from "../Db/ProtocolsClient";
import { ISignalsClient, SignalDescription } from "../Db/SignalsClient";
import { ISignalParser } from "./SignalParser";
import { Protocol, AttributesCollection, FullSignal, TagValue } from "../Contract";

export interface ReadExamination {
    id: string;
    protocol: Protocol;
    tags: AttributesCollection<TagValue>;
    params: AttributesCollection<Object>;
    signals: FullSignal[];
}

export interface IExaminationReader {
    download(protoId: string, examId: string): Promise<ReadExamination>;
}

// Based on https://github.com/maxogden/concat-stream/blob/master/index.js
function isArrayish(arr: any) {
    return /Array\]$/.test(Object.prototype.toString.call(arr));
}

function isBufferish(p: any) {
    return isArrayish(p) || (p && typeof p.subarray === "function");
}

class BufferWritable extends Writable {

    private readonly parts: Buffer[] = [];

    getResult() {
        return Buffer.concat(this.parts);
    }

    _write(chunk: any, encoding: string, callback: Function): void {
        if (Buffer.isBuffer(chunk)) {
            this.parts.push(chunk);
        } else if (typeof chunk === "string") {
            this.parts.push(Buffer.from(chunk, encoding));
        } else if (isBufferish(chunk)) {
            this.parts.push(Buffer.from(chunk));
        } else {
            this.parts.push(Buffer.from(String(chunk)));
        }
        callback();
    }

}

export class ExaminationReader implements IExaminationReader {

    constructor(
        private readonly examinations: IExaminationsClient,
        private readonly protocols: IProtocolsClient,
        private readonly signals: ISignalsClient,
        private readonly signalParser: ISignalParser) {
    }

    async download(protoId: string, examId: string): Promise<ReadExamination> {
        const proto = await this.downloadProtocol(protoId);
        const exam = await this.downloadExam(protoId, examId);
        const data = await Promise.all(exam.signals.map(s => this.downloadSignal({ ...s, protoId, examId })));

        if (!data.every(d => this.signalParser.validate(d.data))) {
            winston.warn("Examination %s, protocol %s has invalid signals, skipping", examId, protoId);
            throw new Error("Not all signals are valid, fix the data first");
        }
        const parsedSignals = data.map(d => ({
            signal: d.signal,
            stage: d.stage,
            data: this.signalParser.parse(d.data)
        }));

        return {
            id: exam.id,
            protocol: proto,
            tags: exam.tags,
            params: exam.params,
            signals: parsedSignals
        };
    }

    async downloadExam(protoId: string, examId: string) {
        const exam = await this.examinations.find(protoId, examId);
        if (exam.type === "DbActionError") {
            winston.warn("Cannot download examination %s, protocol %s, db error: %s", examId, protoId, exam.error);
            throw new Error("Cannot download examination " + examId + " of protocol " + protoId + ", database error occured: " + exam.error);
        } else {
            return exam.result;
        }
    }

    async downloadProtocol(protoId: string) {
        const proto = await this.protocols.find(protoId);
        if (proto.type === "DbActionError") {
            winston.warn("Cannot download protocol %s, db error: %s", protoId, proto.error);
            throw new Error("Cannot download protocol " + protoId + ", database error occured: " + proto.error);
        } else {
            return proto.result;
        }
    }

    async downloadSignal(request: SignalDescription) {
        const output = new BufferWritable();
        const readRequest = { ...request, output };
        const result = await this.signals.download(readRequest);
        if (result.type === "DbActionError") {
            winston.warn("Cannot download signal %s/%s of examination %s, protocol %s, db error: %s",
                request.stage, request.signal, request.examId, request.protoId, result.error);
            throw new Error("Cannot download signal " + request.stage + "/" + request.signal +
                " of examination " + request.examId + " of protocol " + request.protoId +
                ", database error occured: " + result.error);
        } else {
            return { stage: request.stage, signal: request.signal, data: output.getResult() };
        }
    }

}
