import * as React from "react";
import { TouchTapEvent, TouchTapEventHandler } from "material-ui";
import FlatButton from "material-ui/FlatButton";
import IconButton from "material-ui/IconButton";
import Popover from "material-ui/Popover";

type horizontal = "left" | "middle" | "right";
type vertical = "top" | "center" | "bottom";

interface Origin {
    horizontal: horizontal;
    vertical: vertical;
}

interface PopupButtonProps {
    popoverStyle?: React.CSSProperties;
    anchorOrigin: Origin;
    targetOrigin: Origin;
    onClosed: () => void;
}

interface PopupButtonState {
    open: boolean;
    anchorElement?: Element;
}

class PopupButton<P extends PopupButtonProps> extends React.Component<P, PopupButtonState> {

    constructor(props?: P) {
        super(props);

        this.state = {
            open: false,
        };
    }

    handleTouchTap = (event: TouchTapEvent) => {
        // This prevents ghost click.
        event.preventDefault();

        this.setState({
            ...this.state,
            open: true,
            anchorElement: event.currentTarget as Element,
        });
    }

    handleRequestClose = () => {
        this.setState({
            open: false,
        });
        this.props.onClosed();
    }

}


export interface FlatPopupButtonProps extends PopupButtonProps {
    buttonLabel: string;
    buttonStyle?: React.CSSProperties;
}

export class FlatPopupButton extends PopupButton<FlatPopupButtonProps> {

    constructor(props?: FlatPopupButtonProps) {
        super(props);
    }

    render() {
        return (
            <div>
                <FlatButton
                    onTouchTap={this.handleTouchTap}
                    label={this.props.buttonLabel}
                    style={this.props.buttonStyle}
                />
                <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchorElement}
                    anchorOrigin={this.props.anchorOrigin}
                    targetOrigin={this.props.targetOrigin}
                    onRequestClose={this.handleRequestClose}
                    style={this.props.popoverStyle}
                    >
                    {this.props.children}
                </Popover>
            </div>
        );
    }

}

export interface IconPopupButtonProps extends PopupButtonProps {
    iconType: { new(): React.Component<any, any> };
    iconColor?: any;
    tooltip?: string;
}

export class IconPopupButton extends PopupButton<IconPopupButtonProps> {

    constructor(props?: IconPopupButtonProps) {
        super(props);
    }

    render() {
        return (
            <div>
                <IconButton onTouchTap={this.handleTouchTap} tooltip={this.props.tooltip}>
                    <this.props.iconType color={this.props.iconColor}/>
                </IconButton>
                <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchorElement}
                    anchorOrigin={this.props.anchorOrigin}
                    targetOrigin={this.props.targetOrigin}
                    onRequestClose={this.handleRequestClose}
                    style={this.props.popoverStyle}
                    >
                    {this.props.children}
                </Popover>
            </div>
        );
    }

}
