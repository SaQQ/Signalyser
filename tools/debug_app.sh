#!/usr/bin/env bash
pushd `dirname $0` > /dev/null
PWD=$(pwd)
popd > /dev/null
NODE_ENV=development $PWD/node_modules/.bin/electron "$@"
