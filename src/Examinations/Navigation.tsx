import * as React from "react";

import { push } from "react-router-redux";

import { ThunkAction } from "../Utils";

import { AppState } from "../State";

import { loadExamination, newExamination } from "./Editor/Actions";
import { loadProtocol } from "../Protocols/Designer/Actions";

export const SelectExamination = "/protocols/select-examination";
export const EditExamination = "/protocols/edit-examination";

export function openExaminationsList(protocolId: string): ThunkAction<AppState> {
    return dispatch => {
        dispatch(loadProtocol(protocolId));
        dispatch(push(SelectExamination));
    };
}

export function editExamination(protocolId: string, examId: string): ThunkAction<AppState> {
    return dispatch => {
        dispatch(push(EditExamination));
        dispatch(loadExamination(protocolId, examId));
    };
}

export function openNewExamination(protocolId: string): ThunkAction<AppState> {
    return (dispatch, getState) => {
        dispatch(push(EditExamination));
        dispatch(newExamination(protocolId));
    };
}
