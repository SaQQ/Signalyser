import { Action } from "../../../Utils";
import { ExaminationsList, ExaminationsListState } from "../State";
import {
    LoadExaminationsAction, LoadExaminationsResult,
    isLoadExaminations, isLoadExaminationsPending, isLoadExaminationsFulfilled, isLoadExaminationsRejected
} from "./LoadExaminations";

export function loadExaminationsReducer(state: ExaminationsListState, action: Action<LoadExaminationsResult>): ExaminationsListState {
    if (isLoadExaminations(action)) {
        return { ...state, isLoading: true, loadedExaminations: ExaminationsList() };
    } if (isLoadExaminationsPending(action)) {
        return { ...state, isLoading: true, loadedExaminations: ExaminationsList() };
    } else if (isLoadExaminationsFulfilled(action)) {
        return { ...state, isLoading: false, loadedExaminations: ExaminationsList(action.payload) };
    } else if (isLoadExaminationsRejected(action)) {
        return { ...state, isLoading: false, loadedExaminations: ExaminationsList() };
    }
    return state;
}
