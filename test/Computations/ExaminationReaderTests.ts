import { should } from "../Startup";

import { FullSignal, SignalData } from "../../server/Contract";
import { ExaminationReader, ReadExamination } from "../../server/Computations/ExaminationReader";

import * as stub from "./StubData";

describe("Computations/ExaminationReader#download", function () {
    const examsClient = new stub.StubExaminationsClient();
    const signalsClient = new stub.StubSignalsClient();
    const protoClient = new stub.StubProtocolsClient();
    const parser = new stub.StubSignalParser();
    const reader = new ExaminationReader(examsClient, protoClient, signalsClient, parser);

    beforeEach(function () {
        examsClient.reset();
        signalsClient.reset();
        protoClient.reset();
        parser.acceptAll();
    });

    it("downloads the examination", function () {
        return run().should.eventually.be.fulfilled
            .then(() => {
                examsClient.called.should.be.true;
                examsClient.protoId.should.be.equal(stub.protoId);
                examsClient.examId.should.be.equal(stub.examId);
            });
    });

    it("downloads all signals", function () {
        return run().should.eventually.be.fulfilled
            .then(() => {
                const requests = stub.signalDescriptions.map(s => ({
                    protoId: stub.protoId,
                    examId: stub.examId,
                    stage: s.stage,
                    signal: s.signal
                }));
                signalsClient.readCount.should.be.equal(stub.signalDescriptions.length);
                signalsClient.readRequests.should.have.deep.members(requests);
            });
    });

    it("validates correct data", function () {
        const data = ["a", "b"];
        const receivedData: Buffer[] = [];
        parser.onValidate = b => { receivedData.push(b); return true; };
        return runParse(data).should.eventually.be.fulfilled
            .then(() => {
                receivedData.should.be.lengthOf(2);
                receivedData.every(b => b.length === 1).should.be.true;
                should.exist(receivedData.find(e => e.toString() === data[0]));
                should.exist(receivedData.find(e => e.toString() === data[1]));
            });
    });

    it("parses correct data", function () {
        const data = ["a", "b"];
        const receivedData: Buffer[] = [];
        parser.onParse = b => { receivedData.push(b); return stub.realSignal.data; };
        return runParse(data).should.eventually.be.fulfilled
            .then(() => {
                receivedData.should.be.lengthOf(2);
                receivedData.every(b => b.length === 1).should.be.true;
                should.exist(receivedData.find(e => e.toString() === data[0]));
                should.exist(receivedData.find(e => e.toString() === data[1]));
            });
    });

    it("returns parsed signals with correct metadata", function () {
        const signals = stub.signalDescriptions.map(d => ({ ...d, data: stub.makeSignalData() }));
        parser.onParse = b => signals[b.readInt8(0)].data;
        return runParse(["\x00", "\x01"]).should.eventually.be.fulfilled
            .then((e: ReadExamination) => {
                e.signals.should.be.lengthOf(2);
                should.exist(e.signals.find(compareTo(signals[0])));
                should.exist(e.signals.find(compareTo(signals[1])));
            });
    });

    it("returns correct information about the examination", function () {
        return run().should.eventually.be.fulfilled
            .then((e: ReadExamination) => {
                const {signals, ...receivedRest} = e;
                const expectedRest = stub.makeReadExamination(stub.param);
                delete expectedRest.signals;
                receivedRest.should.deep.equal(expectedRest);
            });
    });

    it("throws error when examination retrieval fails", function () {
        return reader.download("", "").should.eventually.be.rejected;
    });

    it("throws error when signal download fails", function () {
        return runWithSignalError().should.eventually.be.rejected;
    });

    it("throws when signal validation fails", function () {
        return runWithParseError().should.eventually.be.rejected;
    });

    it("throws error when protocol retrieval fails", function () {
        return runWithProtocolError().should.eventually.be.rejected;
    });

    function run() {
        examsClient.result = stub.makeExamination();
        signalsClient.signalData = () => "sample-data";
        protoClient.result = stub.protocol;
        parser.acceptAll();
        return reader.download(stub.protoId, stub.examId);
    }

    function runWithSignalError() {
        examsClient.result = stub.makeExamination();
        protoClient.result = stub.protocol;
        parser.acceptAll();
        return reader.download(stub.protoId, stub.examId);
    }

    function runWithParseError() {
        examsClient.result = stub.makeExamination();
        signalsClient.signalData = () => "sample-data";
        protoClient.result = stub.protocol;
        parser.rejectAll();
        return reader.download(stub.protoId, stub.examId);
    }

    function runWithProtocolError() {
        examsClient.result = stub.makeExamination();
        signalsClient.signalData = () => "sample-data";
        parser.acceptAll();
        return reader.download(stub.protoId, stub.examId);
    }

    function runParse(data?: any[]) {
        examsClient.result = stub.makeExamination();
        protoClient.result = stub.protocol;
        if (data !== undefined) {
            signalsClient.signalData = d => data[stub.signalDescriptions.findIndex(e => e.signal === d.signal)];
        } else {
            signalsClient.signalData = () => "";
        }
        return reader.download(stub.protoId, stub.examId);
    }

    function compareTo(s1: FullSignal) {
        return (s2: FullSignal) => s1.signal === s2.signal && s1.stage === s2.stage && s1.data === s2.data;
    }

});
