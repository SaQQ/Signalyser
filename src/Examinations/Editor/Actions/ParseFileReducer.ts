import { List } from "immutable";

import { Action } from "../../../Utils";
import {
    ParseFileResult,
    isParseFilePending, isParseFileFulfilled, isParseFileRejected, isParseFile
} from "./ParseFile";
import { EditingExaminationState } from "../State";

export function parseFileReducer(state: EditingExaminationState, action: Action<ParseFileResult>): EditingExaminationState {
    if (isParseFilePending(action)) {
        return { ...state, isLoading: true, choosingParser: false };
    } else if (isParseFileFulfilled(action)) {
        if (action.payload !== null && action.payload !== undefined) {
            return {
                ...state, isLoading: false,
                parseResult: {
                    parserError: "", displayingParseResult: true,
                    tagsAssignment: action.payload[0],
                    signalAssignment: List(action.payload[1]).map(signal => {
                        // Translate stage/signal names to stage/signal Id's
                        let protoSignal = state.protocol.signals.find(s => s.name === signal.signal);
                        let protoStage = state.protocol.stages.find(s => s.name === signal.stage);
                        if (protoSignal !== undefined && protoStage !== undefined) {
                            return { ...signal, signalName: protoSignal.name, stageName: protoStage.name };
                        } else {
                            return { ...signal, signalName: "", stageName: "" };
                        }
                    }).toList(),
                    selectedPlace: ["", ""], selectedSignal: -1
                }
            };
        } else {
            return {
                ...state, isLoading: false,
                parseResult: {
                    parserError: "No signals returned", displayingParseResult: true,
                    tagsAssignment: {}, signalAssignment: List([]), selectedPlace: ["", ""], selectedSignal: -1
                }
            };
        }
    } else if (isParseFileRejected(action)) {
        return {
            ...state, isLoading: false,
            parseResult: {
                parserError: action.payload, displayingParseResult: true,
                tagsAssignment: {}, signalAssignment: List([]), selectedPlace: ["", ""], selectedSignal: -1
            }
        };
    }
    return state;
}
