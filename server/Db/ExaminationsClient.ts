import * as mongo from "mongodb";
import * as winston from "winston";

import { Examination, ExaminationWithTags, ExaminationDescription } from "../Contract";

import * as db from "./DbClient";
import * as dbSig from "./SignalsClient";
import * as mappers from "./Mappers";

export interface IExaminationsClient {
    all(protoId: string, filter: any): Promise<db.ReadResult<ExaminationDescription[]>>;
    find(protoId: string, id: string): Promise<db.ReadResult<Examination>>;
    add(protoId: string, exam: ExaminationWithTags): Promise<db.CreateResult>;
    update(protoId: string, id: string, exam: ExaminationWithTags): Promise<db.UpdateResult>;

    setParameter(protoId: string, id: string, paramName: string, value: Object): Promise<db.UpdateResult>;

    rename(protoId: string, tags: db.NamedUpdate[], params: db.NamedUpdate[]): Promise<db.CollectionUpdateResult>;
}

export class ExaminationsClient implements IExaminationsClient {

    constructor(private readonly client: db.IDbClient,
        private readonly signals: dbSig.ISignalsClient) { }

    async all(protoId: string, filter: Object) {
        const exams = this.client.examinations(protoId);
        if (exams === null) {
            winston.verbose("Cannot list examinations, protocol %s not found", protoId);
            return db.notFound();
        } else {
            try {
                winston.info("Getting examinations of protocol %s. Filter: %j", protoId, filter);
                return db.read(await exams
                    .find(filter, mappers.Examinations.descriptionProjection)
                    .sort({ name: 1 })
                    .map(mappers.Examinations.dbToDescription)
                    .toArray());
            } catch (e) {
                winston.warn("Cannot list examinations", e);
                return db.internalError();
            }
        }
    }

    async find(protoId: string, id: string) {
        const exams = this.client.examinations(protoId);
        if (exams == null) {
            winston.verbose("Cannot get the examination %s, protocol %s not found", id, protoId);
            return db.notFound();
        } else {
            try {
                const _id = mongo.ObjectID.createFromHexString(id);
                const exam = await exams.findOne({ _id });
                if (exam === null) {
                    return db.notFound();
                } else {
                    const signals = await this.signals.list(protoId, id);
                    if (signals.type === "DbActionError") {
                        return signals;
                    } else {
                        const apiExam = mappers.Examinations.dbToApi(exam, signals.result);
                        return db.read(apiExam);
                    }
                }
            } catch (e) {
                winston.warn("Cannot get examination %s, protocol %s", id, protoId, e);
                return db.internalError();
            }
        }
    }

    async add(protoId: string, exam: ExaminationDescription): Promise<db.CreateResult> {
        const dbExam = mappers.Examinations.apiToDb(exam);
        try {
            const exams = this.client.examinations(protoId);
            if (exams == null) {
                winston.verbose("Cannot insert new examination, protocol %s not found", protoId);
                return db.notFound();
            } else {
                const result = await exams.insertOne(dbExam);
                winston.info("New examination (id: %s) inserted", result.insertedId);
                return db.created(result);
            }
        } catch (e) {
            winston.warn("Cannot insert new examination", e);
            return db.internalError();
        }
    }

    async update(protoId: string, id: string, exam: ExaminationDescription): Promise<db.UpdateResult> {
        const dbExam = mappers.Examinations.apiToDb(exam, id);
        try {
            const exams = await this.client.examinations(protoId);
            if (exams == null) {
                winston.verbose("Cannot update examination %s, protocol %s not found", id, protoId);
                return db.notFound();
            } else {
                const result = await exams.updateOne({ _id: dbExam._id }, dbExam);
                if (result.matchedCount === 0) {
                    winston.verbose("Examination with id %s not found, protocol %s, cannot update", id, protoId);
                    return db.notFound();
                } else {
                    winston.info("Examination with id %s updated", id);
                    return db.updated(dbExam._id);
                }
            }
        } catch (e) {
            winston.warn("Cannot update examination %s, protocol %s", id, protoId, e);
            return db.internalError();
        }
    }

    async setParameter(protoId: string, id: string, paramName: string, value: Object): Promise<db.UpdateResult> {
        const examId = new mongo.ObjectID(id);
        try {
            const exams = await this.client.examinations(protoId);
            if (exams == null) {
                winston.verbose("Cannot add parameter %s to examination %s, protocol %s not found", paramName, id, protoId);
                return db.notFound();
            } else {
                const result = await exams.updateOne({ _id: examId }, {
                    $set: {
                        ["params." + paramName]: value
                    }
                });
                if (result.matchedCount === 0) {
                    winston.verbose("Examination with id %s not found, protocol %s, cannot update", id, protoId);
                    return db.notFound();
                } else {
                    winston.info("Parameter %s added to examination %s, protocol %s", paramName, id, protoId);
                    return db.updated(examId);
                }
            }
        } catch (e) {
            winston.warn("Cannot add parameter %s to examination %s, protocol %s, Mongo returned an error", paramName, id, protoId, e);
            return db.internalError();
        }
    }

    async rename(protoId: string, tags: db.NamedUpdate[], params: db.NamedUpdate[]): Promise<db.CollectionUpdateResult> {
        try {
            const exams = await this.client.examinations(protoId);
            if (exams === null) {
                winston.verbose("Cannot rename tags/params, protocol %s not found", protoId);
                return db.notFound();
            }

            if (tags.length > 0 || params.length > 0) {
                let mongoCommand: { [name: string]: string; } = {};
                tags.forEach(t => mongoCommand["tags." + t.oldName] = "tags." + t.newName);
                params.forEach(p => mongoCommand["params." + p.oldName] = "params." + p.newName);
                await exams.updateMany({}, { "$rename": mongoCommand });
            }

            return db.collectionUpdated();
        } catch (e) {
            winston.warn("Cannot rename tags/params for examinations of protocol %s, Mongo returned an error", protoId, e);
            return db.internalError();
        }
    }
}
