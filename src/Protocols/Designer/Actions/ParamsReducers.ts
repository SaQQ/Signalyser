import * as immutable from "immutable";
import { v1 as uuid } from "uuid";

import * as ts from "typescript";

import { Action, generateUniqueName, validateNamesCollection } from "../../../Utils";

import { ParamActions, isAddNewParam, isRenameParam, isRemoveParam, isSetParamFormula } from "./Params";

import { ParamsState, EditingParamState } from "../State";

const transpileOptions: ts.TranspileOptions = {
    compilerOptions: {
        module: ts.ModuleKind.CommonJS,
        moduleResolution: ts.ModuleResolutionKind.NodeJs,
        strictNullChecks: true
    },
    reportDiagnostics: true
};

const basicFormula = [
    "import { FullExamination } from \"Contract\";",
    "",
    "export default function calculate(examination: FullExamination): Object {",
    "    return 0;",
    "}"
].join("\n");

const basicCompiledFormula = ts.transpileModule(basicFormula, transpileOptions).outputText;

export function paramsReducer(state: ParamsState, action: Action<ParamActions>): ParamsState {
    if (isAddNewParam(action)) {
        let name = generateUniqueName("Param", state);
        return state.push({
            id: uuid(),
            oldName: name, name: name,
            formula: basicFormula, compiledFormula: basicCompiledFormula,
            compileError: false, error: ""
        });
    } else if (isRenameParam(action)) {
        let idx = state.findIndex(s => s.id === action.id);
        let newState = idx !== -1 ? state.update(idx, v => ({ ...v, name: action.newName })) : state;
        return newState.map(param => ({ ...param, error: validateNamesCollection(param, newState) })).toList();
    } else if (isRemoveParam(action)) {
        let newState = state.filterNot(e => e.id === action.id).toList();
        return newState.map(param => ({ ...param, error: validateNamesCollection(param, newState) })).toList();
    } else if (isSetParamFormula(action)) {
        let idx = state.findIndex(s => s.id === action.id);
        let compileResult = ts.transpileModule(action.formula, transpileOptions);
        if (compileResult.diagnostics) {
            if (compileResult.diagnostics.reduce((p, c) => c.category === ts.DiagnosticCategory.Error ? true : p, false)) {
                // If there were errors, set error state
                return state.update(idx, v => ({ ...v, formula: action.formula, compiledFormula: "", compileError: true }));
            }
        }
        return state.update(idx, v => ({ ...v, formula: action.formula, compiledFormula: compileResult.outputText, compileError: false }));
    }
    return state || immutable.List<EditingParamState>();
}
