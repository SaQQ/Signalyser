import { ParserReturnType, SignalAssignment } from "./ParserSignature";

import * as fs from "fs";
import * as csvParse from "csv-parse";

export default (filename: string, resolve: (result: ParserReturnType) => void, reject: (reason?: any) => void) => {

    let parser = csvParse({
        delimiter: ";",
        skip_empty_lines: true,
        trim: true
    }, (err, data) => {
        if (err) {
            reject(err);
        } else {
            let samples = data.length;
            let series = data[0].length - 2; // Time and last column (empty)
            let timeSeries = new Array<number>(samples);
            let dataSeries = new Array<Array<number>>(series);
            for (let i = 0; i < series; i++) {
                dataSeries[i] = new Array(samples);
            }
            for (let i = 0; i < samples; i++) {
                timeSeries[i] = parseFloat(data[i][0]);
                for (let j = 0; j < series; j++) {
                    dataSeries[j][i] = parseInt(data[i][j + 1]);
                }
            }

            resolve([{}, dataSeries.map<SignalAssignment>((data, i) => ({
                description: "Signal " + i,
                data: { timeData: timeSeries, seriesData: [data] },
                signal: "",
                stage: ""
            }))]);
        }
    });

    try {
        fs.createReadStream(filename).pipe(parser);
    } catch (e) {
        reject(e);
    }
};
