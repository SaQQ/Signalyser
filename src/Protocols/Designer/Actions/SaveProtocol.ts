import { remote } from "electron";
import * as immutable from "immutable";

import {
    Action, ThunkAction,
    PromiseAction, PendingPromiseAction, FulfilledPromiseAction, RejectedPromiseAction,
    Named, isValidUserId
} from "../../../Utils";
import { ServerClient, handleClientError } from "../../../Client";

import { AppState } from "../../../State";
import { EditingProtocolState, EditingTagState, EditingParamState } from "../State";
import {
    Protocol, TagDescriptor, ParameterDescriptor,
    CreatedOrUpdated
} from "../../../Contract";

interface SaveProtocolAction extends PromiseAction<CreatedOrUpdated> {
    type: "SaveProtocol";
}
interface SaveProtocolPendingAction extends PendingPromiseAction<CreatedOrUpdated> {
    type: "SaveProtocol_PENDING";
}
interface SaveProtocolFulfilledAction extends FulfilledPromiseAction<CreatedOrUpdated> {
    type: "SaveProtocol_FULFILLED";
}
interface SaveProtocolRejectedAction extends RejectedPromiseAction<CreatedOrUpdated> {
    type: "SaveProtocol_REJECTED";
}
export const isSaveProtocol =
    (a: Action<SaveProtocolAction>): a is SaveProtocolAction => a.type === "SaveProtocol";
export const isSaveProtocolPending =
    (a: Action<SaveProtocolPendingAction>): a is SaveProtocolPendingAction => a.type === "SaveProtocol_PENDING";
export const isSaveProtocolFulfilled =
    (a: Action<SaveProtocolFulfilledAction>): a is SaveProtocolFulfilledAction => a.type === "SaveProtocol_FULFILLED";
export const isSaveProtocolRejected =
    (a: Action<SaveProtocolRejectedAction>): a is SaveProtocolRejectedAction => a.type === "SaveProtocol_REJECTED";

export type SaveProtocolResult = SaveProtocolPendingAction | SaveProtocolFulfilledAction | SaveProtocolRejectedAction;

function mapToSave(state: EditingProtocolState) {
    const signals = state.signals.map(s => ({ name: s.name })).toArray();
    const stages = state.stages.map(s => ({ name: s.name })).toArray();
    const tags = state.tags.map(t => ({ name: t.name })).toArray();
    const params = state.params.map(t => ({
        name: t.name,
        formula: t.formula, compiledFormula: t.compiledFormula
    })).toArray();
    return {
        id: state.id,
        name: state.name,
        signals, stages, tags, params
    };
}

function mapToUpdate(state: EditingProtocolState) {
    const signals = state.signals.map(s => ({ name: s.name, newName: s.name })).toArray();
    const stages = state.stages.map(s => ({ name: s.name, newName: s.name })).toArray();
    const tags = state.tags.map(t => ({ name: t.name, newName: t.name })).toArray();
    const params = state.params.map(t => ({
        name: t.name, newName: t.name,
        formula: t.formula, compiledFormula: t.compiledFormula
    })).toArray();

    return {
        id: state.id,
        name: state.name,
        signals, stages, tags, params
    };
}

export function saveProtocol(): ThunkAction<AppState> {
    return (dispatch, getState) => {
        const state = getState().protocols.editing;
        let promise: Promise<CreatedOrUpdated>;
        if (state.isNew) {
            promise = ServerClient.saveProtocol(mapToSave(state));
        } else {
            promise = ServerClient.updateProtocol(mapToUpdate(state));
        }
        dispatch({ type: "SaveProtocol", payload: promise })
            .catch(handleClientError);
    };
}

