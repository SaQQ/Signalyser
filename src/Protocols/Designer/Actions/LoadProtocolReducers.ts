import * as immutable from "immutable";
import { v1 as uuid } from "uuid";

import { Action } from "../../../Utils";

import {
    LoadProtocolResult,
    isLoadProtocol, isLoadProtocolPending, isLoadProtocolFulfilled, isLoadProtocolRejected
} from "./LoadProtocol";

import { EditingProtocolState } from "../State";

function prepareNamed<T extends { name: string }>(input: T[]) {
    let result = input.map(e => Object.assign(e, { id: uuid(), error: "", oldName: e.name }));
    return immutable.List<T & { id: string, error: string, oldName: string }>(result);
}

function prepareParams<T extends { name: string }>(input: T[]) {
    let result = input.map(e => Object.assign(e, { id: uuid(), error: "", oldName: e.name, compileError: false }));
    return immutable.List<T & { id: string, error: string, oldName: string, compileError: boolean }>(result);
}

export function loadProtocolReducer(state: EditingProtocolState, action: Action<LoadProtocolResult>): EditingProtocolState {
    if (isLoadProtocolPending(action)) {
        return { ...state, isLoading: true, id: action.payload.id };
    } else if (isLoadProtocolFulfilled(action)) {
        return {
            isLoading: false,
            isNew: false,
            id: action.payload.id,
            name: action.payload.name,
            stages: prepareNamed(action.payload.stages),
            signals: prepareNamed(action.payload.signals),
            tags: prepareNamed(action.payload.tags),
            params: prepareParams(action.payload.params)
        };
    } else if (isLoadProtocolRejected(action)) {
        return { ...state, isLoading: false };
    }
    return state;
}
