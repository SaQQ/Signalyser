import * as React from "react";
import * as ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";

import { Store, createStore, applyMiddleware, compose } from "redux";
import promiseMiddleware from "redux-promise-middleware";
import { routerMiddleware } from "react-router-redux";
import thunk from "redux-thunk";

import { Provider, combineReducersInOrder } from "./Utils";

import { Header, LinkButton } from "./Controls";

import { appStateReducers } from "./Reducers";
import { AppState } from "./State";

import { SettingsDialog } from "./Settings/Component";
import * as Protocols from "./Protocols";
import * as Examinations from "./Examinations";

import * as hacks from "./Hacks";
import * as debug from "./DebugPersistStore";

const App = (props: { children: React.ReactNode }) =>
    <MuiThemeProvider>
        <div>
            <SettingsDialog />
            {props.children}
        </div>
    </MuiThemeProvider>;

const composeWithDevTools =
    process.env.NODE_ENV === "development" &&
        typeof window === "object" &&
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) :
        compose;

const reducers = combineReducersInOrder(debug.reducer, appStateReducers);
const store: Store<AppState> = createStore(reducers, composeWithDevTools(
    applyMiddleware(
        // debug.middleware,
        thunk,
        promiseMiddleware(),
        routerMiddleware(hashHistory)
    )
));
debug.prepare(store);
hacks.applyHacks();

ReactDOM.render(
    <Provider store={store}>
        <Router history={hashHistory}>
            <Route path="/" component={App}>
                <IndexRoute component={Protocols.ProtocolSelector} />
                {Protocols.Routes}
                {Examinations.Routes}
            </Route>
        </Router>
    </Provider>,
    document.getElementById("container"));
