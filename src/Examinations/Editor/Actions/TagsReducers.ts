import * as immutable from "immutable";

import { Action } from "../../../Utils";

import { TagAction, isSetTagValue, isAddTagValue } from "./Tags";

import { TagsState } from "../State";

function parseTagValue(str: string): string | number {
    let num = Number(str);
    return isNaN(num) ? str : num;
}

export function tagsReducer(state: TagsState, action: Action<TagAction>): TagsState {
    if (isSetTagValue(action)) {
        let val = parseTagValue(action.newValue);
        return { ...state, [action.name]: val };
    } else if (isAddTagValue(action)) {
        let val = parseTagValue(action.value);
        return { ...state, [action.name]: val };
    }
    return state || {};
}
