import { push, replace } from "react-router-redux";

import { ThunkAction } from "../Utils";

import { AppState } from "../State";
import { loadProtocols } from "./Selector/Actions";
import { newProtocol, loadProtocol } from "./Designer/Actions";

export const ProtocolsList = "/";
export const NewProtocol = "/protocols/new";
export const EditProtocol = "/protocols/edit";

export const RootRoute = ProtocolsList;

export function openProtocolsList(forceReload?: boolean): ThunkAction<AppState> {
    return dispatch => {
        dispatch(replace(ProtocolsList));
        if (forceReload !== undefined && forceReload) {
            dispatch(loadProtocols());
        }
    };
}

export function openNewProtocol(): ThunkAction<AppState> {
    return dispatch => {
        dispatch(push(NewProtocol));
        dispatch(newProtocol());
    };
}

export function editProtocol(id: string): ThunkAction<AppState> {
    return dispatch => {
        dispatch(push(EditProtocol));
        dispatch(loadProtocol(id));
    };
}
