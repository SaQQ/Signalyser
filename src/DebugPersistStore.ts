import { Store, MiddlewareAPI, Dispatch, Action } from "redux";
import { AppState } from "./State";

const InitStateKey = "@@InitState";
const SetStateKey = "@@SetState";
const StoreKey = "REDUX_DEBUG_STORE";

// Session storage may be too small for signal data, simpli replace it with dummy data
function replaceFloat32Arrays(key: string, value: any) {
    if (key === "timeData" || key === "seriesData") {
        return "null";
    }
    return value;
}

function reviveFloat32Arrays(key: string, value: string) {
    if (key === "timeData") {
        return new Float32Array([0, 1]);
    } else if (key === "seriesData") {
        return [new Float32Array([0, 1])];
    }
    return value;
}

const idMiddleware = (store: MiddlewareAPI<AppState>) => (next: Dispatch<AppState>) => <A extends Action>(action: A) => next(action);
const debugPersist = (store: MiddlewareAPI<AppState>) => (next: Dispatch<AppState>) => <A extends Action>(action: A) => {
    if (action && action.type === InitStateKey) {
        let existingState = sessionStorage.getItem(StoreKey);
        if (existingState !== null) {
            try {
                let state = JSON.parse(existingState, reviveFloat32Arrays);
                store.dispatch({ type: "@@SetState", payload: state });
            } catch (e) {
                console.warn("Cannot restore existing state - cannot parse the state", e);
            }
        }
    }

    let result = next(action);
    sessionStorage.setItem(StoreKey, JSON.stringify(store.getState(), replaceFloat32Arrays));
    return result;
};

const initActionReducer = (s: AppState, a: { type: string, payload: AppState }) => {
    if (a.type === SetStateKey) {
        return a.payload;
    }
    return s;
};

const idReducer = (s: AppState, a: Action) => s;

export const middleware = process.env.NODE_ENV === "development" ? debugPersist : idMiddleware;
export const reducer = process.env.NODE_ENV === "development" ? initActionReducer : idReducer;
export const prepare = process.env.NODE_ENV === "development" ?
    (store: Store<AppState>) => store.dispatch({ type: InitStateKey }) :
    (store: Store<AppState>) => { };
