'use strict';

const gulp = require('gulp');
const fs = require('fs');
const path = require('path');

function isDirNewer(dirA, dirB) {
    if (!fs.existsSync(dirA)) {
        return false;
    } else if (!fs.existsSync(dirB)) {
        return true;
    }
    let a = fs.statSync(dirA).mtime.getTime();
    let b = fs.statSync(dirB).mtime.getTime();
    return a > b;
};

function copyOptional(src, dest) {
    if (isDirNewer(path.dirname(src), path.dirname(dest))) {
        return gulp.src(src).pipe(gulp.dest(dest));
    } else {
        return gulp.src([]);
    }
};

module.exports = { isDirNewer, copyOptional };
