/// <reference path="./plotly-fix.d.ts" />

import * as React from "react";
// Importing plotly.js as a file forces it to detect env as browser, not node
// and therefore it does not require gl package
// <That's ugly>
import * as Plotly from "plotly.js/dist/plotly.js";

import { List } from "immutable";

import { SignalData, SignalDescriptor, StageDescriptor } from "../Contract";

import { selectUniqueNamed } from "../Utils";

export class ExaminationPlotProps {
    protocolSignals: ReadonlyArray<SignalDescriptor>;
    protocolStages: ReadonlyArray<StageDescriptor>;
    readonly data: List<{
        stage: string;
        signal: string;
        data: SignalData;
    }>;
}

const layoutLegend: any = {
    traceorder: "reversed",
    x: 0.5,
    y: -0.1,
    orientation: "h"
};

const layoutMargin: any = {
    l: 40,
    r: 40,
    b: 40,
    t: 40
};

export class ExaminationPlot extends React.Component<ExaminationPlotProps, {}> {

    refs: {
        plotDiv: (HTMLDivElement);
    };

    private naxis(dir: "x" | "y", index: number): string {
        return dir + "axis" + (index + 1);
    }

    private naxisShort(dir: "x" | "y", index: number): string {
        return dir + (index + 1);
    }

    shouldComponentUpdate(nextProps: ExaminationPlotProps) {
        return nextProps.data !== this.props.data
            || nextProps.protocolSignals !== this.props.protocolSignals
            || nextProps.protocolStages !== this.props.protocolStages;
    }

    componentDidUpdate() {
        this.draw();
    }

    componentDidMount() {
        this.draw();
    }

    private draw() {

        let { protocolSignals, protocolStages, data } = this.props;

        let signals = protocolSignals.concat(data.map(d => ({ name: d.signal })).toArray());
        let stages = protocolStages.concat(data.map(d => ({ name: d.stage })).toArray());

        signals = selectUniqueNamed(signals);
        stages = selectUniqueNamed(stages);

        let sigCount = signals.length;

        let yPartition = 1.0 / (sigCount === 0 ? 1 : sigCount);

        let layout: any = {
            legend: layoutLegend,
            margin: layoutMargin
        };

        for (let i = 0; i < sigCount; ++i) {
            layout[this.naxis("y", i)] = { domain: [i * yPartition, (i + 1) * (yPartition)] };
            if (i < sigCount - protocolSignals.length) {
                layout[this.naxis("y", i)]["gridwidth"] = 3;
                layout[this.naxis("y", i)]["color"] = "#F9A825";
            }
        }

        let plotData: any = data.reduce((p: any[], d) => {
            let { signal, stage, data } = d;
            let stageIndex = stages.findIndex(s => s.name === stage);
            let signalIndex = signals.findIndex(s => s.name === signal);

            return p.concat(data.seriesData.map(channelData => ({
                y: channelData,
                x: data.timeData,
                yaxis: this.naxisShort("y", sigCount - signalIndex - 1),
                mode: "lines",
                type: "scatter",
                name: signal
            })));
        }, []);

        Plotly.newPlot(this.refs.plotDiv, plotData, layout, { showLink: false });
    }

    render() {
        return <div ref="plotDiv" className="plot" />;
    }

}

export interface SinglePlotProps {
    readonly timeSeries: ArrayLike<number>;
    readonly dataSeries: ArrayLike<number>[];
}

export class SinglePlot extends React.Component<SinglePlotProps, {}> {

    refs: {
        plotDiv: HTMLElement;
    };

    componentDidMount() {
        let traces = this.props.dataSeries.map(data => ({
            y: data,
            x: this.props.timeSeries,
            mode: "lines",
            type: "scatter"
        }));

        Plotly.newPlot(this.refs.plotDiv, traces);
    }

    render() {
        return <div ref="plotDiv" className="plot" />;
    }
}

